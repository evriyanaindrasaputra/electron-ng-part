// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.

const imageWrapper = document.getElementById("image-wrapper");
const selectedNG = JSON.parse(localStorage.getItem("selectedNG"))
  ? JSON.parse(localStorage.getItem("selectedNG"))
  : [];

if (selectedNG.length > 0) {
  selectedNG.forEach((ng) => {
    const dotNGelement = document.createElement("div");
    dotNGelement.classList.add("dot-ng");

    dotNGelement.id = ng.id;
    dotNGelement.style.transform = `translate(${ng.offsetX}px, ${ng.offsetY}px)`;
    const ngParagraph = document.createElement("p");
    ngParagraph.classList.add("dot-list");
    ngParagraph.innerHTML = ng.name;
    dotNGelement.appendChild(ngParagraph);
    imageWrapper.appendChild(dotNGelement);
  });
}

imageWrapper.addEventListener("click", (e) => {
  if (
    !e.target.classList.contains("dot-ng") &&
    !e.target.classList.contains("dot-list")
  ) {
    let spotNG = {
      id: new Date().getMilliseconds(),
      offsetX: Number(e.offsetX - 20),
      offsetY: Number(e.offsetY - 15),
    };

    const dotNGelement = document.createElement("div");
    dotNGelement.classList.add("dot-ng");

    dotNGelement.id = spotNG.id;
    dotNGelement.style.transform = `translate(${spotNG.offsetX}px, ${spotNG.offsetY}px)`;

    e.target.parentElement.appendChild(dotNGelement);
    // save to local storage from spotNG
    localStorage.setItem("spotNG", JSON.stringify(spotNG));
    setTimeout(() => {
      // push to ng-menu
      window.location.href = "ng-menu.html";
    }, 500);
    spotNG = {
      id: 0,
      offsetX: 0,
      offsetY: 0,
    };
  } else if (e.target.classList.contains("dot-ng")) {
    const selectedNGStorage = JSON.parse(localStorage.getItem("selectedNG"));
    // remove from selectedNGStorage
    const id = Number(e.target.id);
    const nweSelectNG = selectedNGStorage.filter((ng) => ng.id !== id);
    localStorage.setItem("selectedNG", JSON.stringify(nweSelectNG));
    e.target.remove();
  }
  // push to the next page
  // window.location.href = "ng-menu.html";
});
