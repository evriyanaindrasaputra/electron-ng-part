const LISTNG = [
  { id: 1, name: "Warna" },
  { id: 2, name: "Gelombang" },
  { id: 3, name: "Penyok" },
  { id: 4, name: "Goresan" },
  { id: 5, name: "Kasar" },
];

const listNG = document.getElementById("list-ng");
LISTNG.forEach((ng) => {
  const notGood = document.createElement("li");
  notGood.classList.add("list-ng");
  notGood.id = ng.id;
  notGood.innerHTML = ng.name;
  listNG.appendChild(notGood);
});

listNG.addEventListener("click", (e) => {
  const target = e.target;
  if (target.classList.contains("list-ng")) {
    const selectedNGStorage = JSON.parse(localStorage.getItem("selectedNG"))
      ? JSON.parse(localStorage.getItem("selectedNG"))
      : [];
    const spotNG = JSON.parse(localStorage.getItem("spotNG"));
    const selectedNG = LISTNG.find((ng) => ng.id === Number(target.id));
    selectedNGStorage.push({ ...selectedNG, ...spotNG });
    localStorage.setItem("selectedNG", JSON.stringify(selectedNGStorage));
    window.location.href = "index.html";
  }
});
